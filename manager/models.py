from django.db import models

class LocationBarcode(models.Model):
    barcode = models.CharField(max_length=255)

    def __str__(self):
        return self.barcode
    
    class Meta: 
        verbose_name = "Location Barcode"
        verbose_name_plural = "Location Barcodes"