from django.contrib import admin

from manager.models import LocationBarcode


class LocationBarcodeAdmin(admin.ModelAdmin):

    list_display = ["id", "barcode"]

admin.site.register(LocationBarcode, LocationBarcodeAdmin)
