"""recom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from manager import views

app_name = 'manager'

urlpatterns = [

    url(r'^dashboard/$', views.DashboardCBV.as_view(), name="dashboard"),
    url(r'^search/$', views.SearchCBV.as_view(), name="search"),
    url(r'^pickups/$', views.PickupCBV.as_view(), name="pickups"),
    url(r'^approve/(?P<pk>\d+)/$', views.ApproveCBV.as_view(), name="approve"),
    url(r'^approve/(?P<pk>\d+)/location/(?P<location_id>\d+)/$', views.ApproveCBV.as_view(), name="approve"),
    url(r'^dispatch/(?P<pk>\d+)/$', views.DispatchCBV.as_view(), name="dispatch"),
    url(r'^about/$', views.AboutCBV.as_view(), name="about"),

]
