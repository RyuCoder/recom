
from django.views.generic import TemplateView, CreateView, ListView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse_lazy
from django.shortcuts import redirect

from manager.forms import RecordForm
from manager.models import LocationBarcode

from user.models import Record, Department, RecordStatus


class DashboardCBV(TemplateView):
    template_name = 'manager/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardCBV, self).get_context_data(**kwargs)

        context['records'] = Record.objects.count()
        context['joining'] = "00"
        context['title'] = "Dashboard"

        return context


class SearchCBV(ListView):
    template_name = 'manager/search.html'
    model = Record
    context_object_name = "records"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(SearchCBV, self).get_context_data(**kwargs)

        context["departments"] = Department.objects.all()
        context["recordStatus"] = RecordStatus.objects.all()
        context['title'] = "Search"

        return context

    def get_queryset(self):
        # records = Record.objects.filter(status=3)
        records = Record.objects.all()
        
        barcode = self.request.GET.get("barcode", '')
        reference = self.request.GET.get("reference", '')
        dept = self.request.GET.get("dept", 'none')
        status = self.request.GET.get("status", 'none')

        if barcode != "":
            records = records.filter(barcode__iexact = barcode.strip())

        if reference != "":
            records = records.filter(reference__iexact=reference.strip())

        if dept != "none":
            records = records.filter(dept=dept.strip())

        if status != "none":
            records = records.filter(status=status.strip())

        return records


class PickupCBV(ListView):
    template_name = 'manager/pickup.html'
    model = Record
    context_object_name = "records"
    queryset = Record.objects.filter(status__in=[1,3])

    def get_context_data(self, **kwargs):
        context = super(PickupCBV, self).get_context_data(**kwargs)
        context['title'] = "Pickups"
        context['locations'] = LocationBarcode.objects.all()
        return context


class ApproveCBV(View):

    def get(self, *args, **kwargs):
        record_id = kwargs["pk"]
        location_id = kwargs["location_id"]
        status = RecordStatus.objects.get(id=2)
        location = LocationBarcode.objects.get(id=location_id)
        record = Record.objects.get(pk=record_id)
        record.status = status
        record.location = location
        record.save()

        return redirect("manager:search")


class DispatchCBV(View):

    def get(self, *args, **kwargs):
        id = kwargs["pk"]

        record = Record.objects.get(pk=id)
        record.status = RecordStatus.objects.get(id=4)
        record.location = None
        record.save()

        return redirect("manager:search")


class AboutCBV(TemplateView):
    template_name = 'manager/about.html'

    def get_context_data(self, **kwargs):
        context = super(AboutCBV, self).get_context_data(**kwargs)
        context['title'] = "About"
        return context
