from django import forms 
from user.models import Record, RecordStatus


class RecordForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RecordForm, self).__init__(*args, **kwargs)
        # self.initial['status'] = RecordStatus.objects.get(id=self.initial['status']).status

    class Meta():
        model = Record
        fields = ["barcode", "reference", "description", "dept", "status"]
        widgets = {
            'barcode': forms.TextInput(attrs={'class': 'form-control'}),
            'reference': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'dept': forms.Select(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'})
        }
