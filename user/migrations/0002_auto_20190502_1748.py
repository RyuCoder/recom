# Generated by Django 2.2 on 2019-05-02 17:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BoxBarcode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('barcode', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Box',
                'verbose_name_plural': 'Boxes',
            },
        ),
        migrations.AddField(
            model_name='record',
            name='box_barcode',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='user.BoxBarcode', verbose_name='Box'),
        ),
    ]
