
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, View
from django.urls import reverse_lazy, reverse
from django.shortcuts import redirect

from user.forms import RecordForm
from user.models import Record, Department, RecordStatus


class DashboardCBV(TemplateView):
    template_name = 'user/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardCBV, self).get_context_data(**kwargs)

        context['records'] = Record.objects.count()
        context['joining'] = "00"
        context['title'] = "Dashboard"

        return context


from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class SearchCBV(ListView):
    template_name = 'user/search.html'
    model = Record
    context_object_name = "records"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(SearchCBV, self).get_context_data(**kwargs)

        context["departments"] = Department.objects.all()
        context["recordStatus"] = RecordStatus.objects.all()
        context['title'] = "Search"

        return context

    def get_queryset(self):
        records = Record.objects.all()
        
        barcode = self.request.GET.get("barcode", '')
        reference = self.request.GET.get("reference", '')
        dept = self.request.GET.get("dept", 'none')
        status = self.request.GET.get("status", 'none')

        if barcode != "":
            records = records.filter(barcode__iexact = barcode.strip())

        if reference != "":
            records = records.filter(reference__iexact=reference.strip())

        if dept != "none":
            records = records.filter(dept=dept.strip())

        if status != "none":
            records = records.filter(status=status.strip())

        return records


class RecordsCBV(CreateView):
    template_name = 'user/records.html'
    form_class = RecordForm
    success_url = reverse_lazy("user:records")
    initial = {"dept": 1, "status": 1}

    def get_context_data(self, **kwargs):
        context = super(RecordsCBV, self).get_context_data(**kwargs)
        context['title'] = "Records"
        return context


class PickupCBV(View):

    def get_context_data(self, **kwargs):
        context = super(PickupCBV, self).get_context_data(**kwargs)
        context['title'] = "Pickups"
        return context

    def get(self, *args, **kwargs):
        id = kwargs["pk"]

        record = Record.objects.get(pk=id)
        record.status = RecordStatus.objects.get(id=6)
        record.save()

        return redirect("user:search")


class RetrieveCBV(View):

    def get(self, *args, **kwargs):
        record_id = kwargs["pk"]

        record = Record.objects.get(pk=record_id)
        record.status = RecordStatus.objects.get(id=4)
        record.location = None
        record.save()

        return redirect("user:search")
        

class AboutCBV(TemplateView):
    template_name = 'user/about.html'

    def get_context_data(self, **kwargs):
        context = super(AboutCBV, self).get_context_data(**kwargs)
        context['title'] = "About"
        return context
