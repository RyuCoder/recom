from django.db import models

from manager.models import LocationBarcode


class Department(models.Model):
    dept = models.CharField(max_length=255, verbose_name="Department")

    def __str__(self):
        return self.dept


class RecordStatus(models.Model):
    status =  models.CharField(max_length=255, verbose_name="Status")

    def __str__(self):
        return self.status
    
    class Meta:
        verbose_name = "Record Status"
        verbose_name_plural = "Record Status"


class BoxBarcode(models.Model):
    
    barcode = models.CharField(max_length=255)

    def __str__(self):
        return self.barcode 

    class Meta:
        verbose_name = "Box"
        verbose_name_plural = "Boxes"


class Record(models.Model):

    barcode = models.CharField(max_length=255)
    reference = models.CharField(max_length=255)
    description = models.TextField()
    box_barcode = models.ForeignKey(BoxBarcode,
                               on_delete=models.CASCADE,
                               verbose_name="Box")

    dept = models.ForeignKey(Department, 
                        on_delete=models.CASCADE, 
                        verbose_name="Department")

    status = models.ForeignKey(RecordStatus,
                                on_delete=models.CASCADE,
                                verbose_name="Status")

    location = models.ForeignKey(LocationBarcode,
                               on_delete=models.CASCADE,
                               verbose_name="Location Barcode",
                               null=True, blank=True)

    def __str__(self):
        return self.reference

