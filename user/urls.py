"""recom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from user import views 

app_name = 'user'

urlpatterns = [

    url(r'^dashboard/$', views.DashboardCBV.as_view(), name="dashboard"),
    url(r'^search/$', views.SearchCBV.as_view(), name="search"),
    url(r'^records/$', views.RecordsCBV.as_view(), name="records"),
    url(r'^pickups/(?P<pk>\d+)/$', views.PickupCBV.as_view(), name="pickups"),
    url(r'^retrieve/(?P<pk>\d+)/$', views.RetrieveCBV.as_view(), name="retrieve"),
    url(r'^about/$', views.AboutCBV.as_view(), name="about"),

]
