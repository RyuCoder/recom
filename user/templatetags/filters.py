from django import template

register = template.Library()


@register.filter
def twodigits(value):
    """Converts 0-9(single digits) to 00-09 (two digits)"""
    digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    if value in digits:
        return "0" + str(value)
    else:
        return value


@register.filter
def dirs(value):
    return str(dir(value))


@register.filter
def getType(value):
    return str(type(value))
