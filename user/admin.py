from django.contrib import admin

from user.models import Record, RecordStatus, Department, BoxBarcode


class DepartmentAdmin(admin.ModelAdmin):

    search_fields = ['id', 'dept']

    #  For list view
    list_display = ['id', 'dept']

    #  For detail view
    fields = ['id', 'dept']

    # Admin should not be able to modify these fields
    readonly_fields = ['id']

    class Meta:
        model = Department


class RecordAdmin(admin.ModelAdmin):

    search_fields = ['id', 'barcode', "reference", "description"]

    #  For list view
    list_display = ['id', 'barcode', "reference", "box_barcode", "location", 
                    "description", "dept", "status"]

    #  For detail view
    fields = ['id', 'barcode', "reference", "description",
              "box_barcode", "location", "dept", "status"]

    # Admin should not be able to modify these fields
    readonly_fields = ['id']


class RecordStatusAdmin(admin.ModelAdmin):
    search_fields = ['id', 'status']

    #  For list view
    list_display = ['id', 'status']

    #  For detail view
    fields = ['id', 'status']

    # Admin should not be able to modify these fields
    readonly_fields = ['id']


class BoxBarcodeAdmin(admin.ModelAdmin):

    list_display = ['id', 'barcode']


admin.site.site_header = "IKONRecoM Admin Panel"
admin.site.register(Record, RecordAdmin)
admin.site.register(RecordStatus, RecordStatusAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(BoxBarcode, BoxBarcodeAdmin)

